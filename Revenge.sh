#!/bin/bash


function countdown () 
    { 
        if (($# != 1)) || [[ $1 = *[![:digit:]]* ]]; then
            echo "Usage: countdown seconds";
            return;
        fi;
        local t=$1 remaining=$1;
        SECONDS=0;
        while sleep .2; do
	    printf '\rSeconds remaining to Revenge: '"$remaining"' ';
            if (( (remaining=t-SECONDS) <=0 )); then
                printf '\rseconds remaining to proceed' 0;
                break;
            fi;
        done
    }



while true; do 
	countdown $[ ( $RANDOM % 900 )  + 300 ]
	osascript -e 'set volume output volume 80'
	afplay ./Sounds/$(ls -l ./Sounds | awk '/wav/{print $9}'| awk "NR==$(($RANDOM % $(ls -l ./Sounds | grep wav | wc -l)))")	
done
